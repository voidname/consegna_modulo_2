using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{


    Animator anim;
    [SerializeField] Sprite[] sprites;
    SpriteRenderer spriteRenderer;
    [SerializeField] RuntimeAnimatorController movementAnim;
    [SerializeField] RuntimeAnimatorController deathAnim;

    Vector2 lastDirection;
    bool playerIsAlive;

    private enum SpriteReference
    {
        UP, DOWN, LEFT, RIGHT
    }

    private Dictionary<Vector2, Sprite> directionToSprite;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        anim.runtimeAnimatorController = movementAnim;
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        InitializeDirectionToSpriteMap();
        playerIsAlive = true;
    }

    private void OnEnable()
    {
        PlayerController.OnStartDeathAnimation += DeathAnimation;
    }

    private void OnDisable()
    {
        PlayerController.OnStartDeathAnimation -= DeathAnimation;
    }

    private void InitializeDirectionToSpriteMap() 
    {
        directionToSprite = new Dictionary<Vector2, Sprite>
    {
        { Vector2.up, sprites[(int)SpriteReference.UP] },
        { Vector2.down, sprites[(int)SpriteReference.DOWN] },
        { Vector2.left, sprites[(int)SpriteReference.LEFT] },
        { Vector2.right, sprites[(int)SpriteReference.RIGHT] }
    };
    }

    private void Update()
    {
        if (playerIsAlive)
        {
            if (InputManager.IsMoving(out Vector2 direction))
            {
                lastDirection = direction;
                HandleSpriteAnimations(direction);
                anim.enabled = true;
            }
            else
            {
                HandleSpriteAnimations(Vector2.zero);
                anim.enabled = false;
            }
        }
    }

    void HandleSpriteAnimations(Vector2 direction)
    {
        if (direction != Vector2.zero)
        {
            anim.SetFloat("X", direction.x);
            anim.SetFloat("Y", direction.y);
            
        }
        else
        {
            if (lastDirection != Vector2.zero)
            {
                spriteRenderer.sprite = directionToSprite[lastDirection];
            }
            anim.SetFloat("X", 0);
            anim.SetFloat("Y", 0);
        }
    }

    void DeathAnimation()
    {
        playerIsAlive = false;
        anim.enabled = true;
        anim.runtimeAnimatorController = deathAnim;
        float time = anim.runtimeAnimatorController.animationClips[0].length;
        StartCoroutine(DeathAnimationTime(time));
    }

    IEnumerator DeathAnimationTime(float time)
    {
        yield return new WaitForSeconds(time);
        GameManager.OnPlayerDead?.Invoke();
    }
}
            
