using System.Collections;
using System.Collections.Generic;

using UnityEngine;


public class BombController : MonoBehaviour
{
    [SerializeField] Sprite[] BombSprites;
    [SerializeField] int explosionSize = 2;
    [SerializeField] LayerMask destructible;
    [SerializeField] LayerMask playerLayer;
    [SerializeField] LayerMask stage;
    [SerializeField] Transform[] explosionPoints;

    SpriteRenderer SpriteRenderer;

    public float fuseTime;

    private void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();

    }


    private IEnumerator Start()
    {
        float interval = fuseTime / BombSprites.Length;
        int index = 0;
        while (index < BombSprites.Length)
        {
            SpriteRenderer.sprite = BombSprites[index];
            index++;
            yield return new WaitForSeconds(interval);
        }
        Explode();
    }


    void Explode()
    {
        Collider2D[] destructiblesY = Physics2D.OverlapBoxAll(transform.position, new Vector2(0, explosionSize), 0, destructible | playerLayer);
        Collider2D[] destructiblesX = Physics2D.OverlapBoxAll(transform.position, new Vector2(explosionSize, 0), 0, destructible | playerLayer);
        Debug.DrawRay(transform.position, Vector2.up * explosionSize / 2, Color.magenta, 3);
        List<Collider2D> allDestructibles = new();
        allDestructibles.AddRange(destructiblesY);
        allDestructibles.AddRange(destructiblesX);

        foreach (Transform whereToAnimate in explosionPoints)
        {
            if (!Physics2D.OverlapCircle(whereToAnimate.position, 0.3f, stage))
            {
                VFX_manager.instance.PlayAnimation(whereToAnimate);
            }

        }
        foreach (Collider2D col in allDestructibles)
        {
            col.gameObject.TryGetComponent(out IDestructible destructible);
            destructible.GetBombed();
        }

        Destroy(gameObject);






    }

    // DUE ORE PER FARE STA COSA E POI SCOPRO CHE POSSO TILEMAPPARE I GAMEOBJECT

    //void Explode()
    //{
    //    ExplodeInDirection(transform.position, new Vector2(4, 1));
    //    ExplodeInDirection(transform.position, new Vector2(1, 4)); 

    //    Destroy(gameObject); 
    //}


    //void ExplodeInDirection(Vector2 explosionCenter, Vector2 sizeVector)
    //{
    //    Collider2D[] hitColliders = Physics2D.OverlapBoxAll(explosionCenter, sizeVector, 0, destructible);
    //    foreach (Collider2D hitCollider in hitColliders)
    //    {
    //        if (hitCollider.gameObject.TryGetComponent<Tilemap>(out var tilemap))
    //        {
    //            Vector3Int cellPosition = tilemap.WorldToCell(explosionCenter);

    //            for (int i = -explosionSize; i <= explosionSize; i++)
    //            {
    //                if (sizeVector.x > sizeVector.y)
    //                {
    //                    tilemap.SetTile(new Vector3Int(cellPosition.x + i, cellPosition.y, cellPosition.z), null);
    //                }
    //                else
    //                {
    //                    tilemap.SetTile(new Vector3Int(cellPosition.x, cellPosition.y + i, cellPosition.z), null);
    //                }
    //            }
    //        }
    //    }
    //}






}
