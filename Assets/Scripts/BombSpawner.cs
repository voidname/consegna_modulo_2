
using UnityEngine;

public class BombSpawner : MonoBehaviour
{
    [SerializeField] GameObject BombObj;

    private void OnEnable()
    {
        InputManager.actionMap.Player.Bomb.performed += ReleaseBomb;
    }

    private void OnDisable()
    {
        InputManager.actionMap.Player.Bomb.performed -= ReleaseBomb;
    }

    private void ReleaseBomb(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        float roundX = Mathf.Round(transform.position.x + 0.5f) - 0.5f;
        float roundY = Mathf.Round(transform.position.y + 0.5f) - 0.5f;
        Vector2 spawnPosition = new(roundX, roundY);
        Instantiate(BombObj, spawnPosition, Quaternion.identity);
    }
}
