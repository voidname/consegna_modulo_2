using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bricks : MonoBehaviour, IDestructible
{
    Animator animator;
    float destroyDelay = 0f;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        destroyDelay = animator.runtimeAnimatorController.animationClips[1].length;
    }
    public void GetBombed()
    {
        animator.SetBool("Dissolve", true);
        Destroy(gameObject, destroyDelay);
    }
}
