
using UnityEngine;

public class Enemy : MonoBehaviour , IDestructible
{
    [Header("Enemy Settings")]
    [SerializeField] LayerMask stage;
    [SerializeField] LayerMask bomb;
    [SerializeField] float enemySpeed = 5;
    [SerializeField] Transform targetMovePoint;
    [SerializeField] float moveInterval = 2.0f;
    Animator animator;
    Collider2D enemyCollider;
    Vector2 currentDirection;
    float timeSinceLastMove;


    private void Start()
    {
        enemyCollider = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        targetMovePoint.parent = null;
        timeSinceLastMove = moveInterval;
        ChooseNewDirection();
    }

    private void Update()
    {
        timeSinceLastMove += Time.deltaTime;
        if (timeSinceLastMove >= moveInterval )
        {
            timeSinceLastMove = 0;
            ChooseNewDirection();
        }

        MoveEnemy();
    }


    private void MoveEnemy()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetMovePoint.position, enemySpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, targetMovePoint.position) == 0  && !DirectionIsAvailable(currentDirection))
        {
            Vector3 newPosition = targetMovePoint.position + (Vector3)currentDirection;

            targetMovePoint.position = newPosition;
        }
    }
            
            
            
        

    private void ChooseNewDirection()
    {
        Vector2[] allDirections = {Vector2.up,Vector2.down,Vector2.left,Vector2.right};
        int randomizer = Random.Range(0, allDirections.Length);
        Vector2 newDirection = allDirections[randomizer];
        if (!DirectionIsAvailable(newDirection))
        {
            currentDirection = newDirection;
        }
            
    }

    private bool DirectionIsAvailable(Vector2 newInputVector)
    {
        return Physics2D.OverlapCircle(newInputVector + (Vector2)transform.position, 0.3f, stage );
    }

    public void GetBombed()
     {
        GameManager.OnEnemyDead?.Invoke();
        enemyCollider.enabled = false;
        animator.SetBool("EnemyDead", true);
        float time = animator.runtimeAnimatorController.animationClips[1].length;
        Destroy(gameObject, time);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<IDestructible>(out IDestructible uselessVariable)) // come posso evitare sta variabile inutile senza usare il tag o il get component?
        {
            ChooseNewDirection();
        }
    }


}





