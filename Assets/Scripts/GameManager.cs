using System;

using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Events
    public static Action OnEnemyDead;
    public static Action OnVictory;
    public static Action OnPlayerDead;
    // Var
    public static int EnemiesToKill;
    public static int EnemiesKilled;
    

    private void OnEnable()
    {
        OnEnemyDead += EnemyKilled;
        OnPlayerDead += StopTime;
    }

    private void OnDisable()
    {
        OnEnemyDead -= EnemyKilled;
        OnPlayerDead -= StopTime;
    }

    void Start()
    {
        Application.targetFrameRate = 60;
        EnemiesToKill = GetEnemyQuantity();
        EnemiesKilled = 0;
        UI_manager.UpdateUI?.Invoke();
        Time.timeScale = 1;
    }

    void EnemyKilled()
    {
        EnemiesKilled++;
        EnemiesToKill--;
        UI_manager.UpdateUI?.Invoke();
        if (EnemiesToKill == 0) { OnVictory?.Invoke(); }
    }

     int GetEnemyQuantity()
     {
        return GameObject.FindObjectsOfType<Enemy>().Length;
     }

    void StopTime()
    {
        Time.timeScale = 0;
        InputManager.actionMap.Player.Disable();
    }


    
}
