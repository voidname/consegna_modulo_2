using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputManager 
{
    public static ActionMap actionMap;

    static InputManager()
    {  
        actionMap = new ActionMap();
        actionMap.Enable();
    }

    public static Vector2 Movement => actionMap.Player.Movement.ReadValue<Vector2>();

    public static bool IsMoving(out Vector2 direction)
    {
        direction = Movement;
        return direction != Vector2.zero;
    }
}
