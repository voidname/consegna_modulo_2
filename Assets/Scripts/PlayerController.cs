
using UnityEngine;


public class PlayerController : MonoBehaviour , IDestructible
{
    public delegate void PlayerState();
    public static event PlayerState OnStartDeathAnimation;

    [Header("Player")]
    [SerializeField] LayerMask stage;
    [SerializeField] float speed = 5;
    [SerializeField] Transform targetMovePoint;

    [Header("References")]
    [SerializeField] GameObject Bomb;

    Vector2 InputVector;
    Collider2D playerCollider;

    private void Awake()
    {
        playerCollider = GetComponent<Collider2D>();
    }



    private void OnEnable()
    {
        GameManager.OnVictory += DisableInputs;
    }

    private void OnDisable()
    {
        GameManager.OnVictory -= DisableInputs;
    }

    private void Start()
    {
        InputManager.actionMap.Player.Enable();
        targetMovePoint.parent = null;
    }




    private void Update()
    {
        // movement
        InputVector = InputManager.Movement;
        Vector2 newInputVector = InputManager.Movement;

        transform.position = Vector3.MoveTowards(transform.position, targetMovePoint.position, speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, targetMovePoint.position) < 0.05)
        {
            if (!AxisBlocked(newInputVector))
            {
                Vector3 newPosition = targetMovePoint.position + (Vector3)newInputVector;

                if (!Physics2D.OverlapCircle(newPosition, 0.3f, stage))
                {
                    targetMovePoint.position = newPosition;
                    InputVector = newInputVector;
                }
            }
        }
    }

        

    private bool AxisBlocked(Vector2 newInputVector)
    {
        Vector3 xCheck = targetMovePoint.position + new Vector3(newInputVector.x, 0, 0);
        Vector3 yCheck = targetMovePoint.position + new Vector3(0, newInputVector.y, 0);

        return (Mathf.Abs(newInputVector.x) > 0 && Mathf.Abs(InputVector.y) > 0 && Physics2D.OverlapCircle(xCheck, 0.3f, stage)) ||
               (Mathf.Abs(newInputVector.y) > 0 && Mathf.Abs(InputVector.x) > 0 && Physics2D.OverlapCircle(yCheck, 0.3f, stage));
    }

    public void GetBombed()
    {
        playerCollider.enabled = false;
        DisableInputs();
        OnStartDeathAnimation();
    }

    void DisableInputs()
    {
        InputManager.actionMap.Player.Disable();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.TryGetComponent(out Enemy uselelessVariable))
        {
            playerCollider.enabled = false;
            DisableInputs();
            OnStartDeathAnimation();
        }

    }


}
    
