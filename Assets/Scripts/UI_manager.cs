using System;


using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class UI_manager : MonoBehaviour
{
    
    public static Action UpdateUI;

    //Ref
    [SerializeField] TMP_Text EnemyToKill;
    [SerializeField] TMP_Text EnemyKilled;
    [SerializeField] GameObject VictoryScren;
    [SerializeField] GameObject LoseScreen;
    [SerializeField] GameObject PauseMenu;
    bool onPause;

    private void OnEnable()
    {
        UpdateUI += UpdateCounts;
        GameManager.OnPlayerDead += GameOver;
        GameManager.OnVictory += ShowVictoryScreen;
        InputManager.actionMap.Pause.PauseAction.performed += Pause;
    }

    private void OnDisable()
    {
        GameManager.OnPlayerDead -= GameOver;
        UpdateUI -= UpdateCounts;
        GameManager.OnVictory -= ShowVictoryScreen;
    }

    private void Start()
    {
        VictoryScren.SetActive(false);
        LoseScreen.SetActive(false);
        PauseMenu.SetActive(false);
    }
    void GameOver()
    {
        LoseScreen.SetActive(true);
    }

    void UpdateCounts()
    {
        EnemyToKill.text = "Enemies To Kill: " + GameManager.EnemiesToKill.ToString();

        EnemyKilled.text = "Enemies Killed: " + GameManager.EnemiesKilled.ToString();
    }
        

    

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }

    void ShowVictoryScreen()
    {
        VictoryScren.SetActive(true);
    }

    void Pause(InputAction.CallbackContext p)
    {
        onPause = !onPause;
        if(onPause) 
        {
            InputManager.actionMap.Player.Disable();
            Time.timeScale = 0;
            PauseMenu.SetActive(true) ;
        }
        else
        {
            InputManager.actionMap.Player.Enable();
            Time.timeScale = 1;
            PauseMenu.SetActive(false);
        }
    }

    public void Resume()
    {
        onPause = !onPause;
        InputManager.actionMap.Player.Enable();
        PauseMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
