using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_manager : MonoBehaviour
{
    public static VFX_manager instance;
    [SerializeField] Animation explodeAnimation;

    private void Awake()
    {
        instance = this;
    }

    public void PlayAnimation(Transform allTranforms)
    {
        GameObject animationObj = ObjectPooler.SharedInstance.GetPooledObject();
        float animationTime = animationObj.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;
        animationObj.transform.position = allTranforms.position;
        animationObj.SetActive(true);
        StartCoroutine(Deactivate(animationObj, animationTime));
    }
        
        

    IEnumerator Deactivate(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        if(obj != null) { obj.SetActive(false); }
    }
        


}
